This is code for a seasonal fractional snow-covered area algorithm in any snow cover model. 

The code reads snow depth (HS) and snow water equivalent (SWE) data from file HS_SWE.txt and writes the computed fractional snow-covered area (fSCA) to file fSCA.txt.

Running the code:

./my_compil

./main_fSCA


Current reference:
Helbig, N., Schirmer, M., Magnusson, J., Mäder, F., van Herwijnen, A., Quéno, L., Bühler, Y., Deems, J. S., and Gascoin, S.: A seasonal algorithm of the snow-covered area fraction for mountainous terrain, The Cryosphere Discuss. [preprint], https://doi.org/10.5194/tc-2020-377, in review, 2021. 

